import Phaser from 'phaser';
import backyard from '../../assets/images/backyard2.png';
import beanBlue from '../../assets/images/bean_blue.png';
import beanDead from '../../assets/images/bean_dead.png';
import beanGreen from '../../assets/images/bean_green.png';
import beanOrange from '../../assets/images/bean_orange.png';
import beanPink from '../../assets/images/bean_pink.png';
import beanPurple from '../../assets/images/bean_purple.png';
import beanRed from '../../assets/images/bean_red.png';
import beanWhite from '../../assets/images/bean_white.png';
import beanYellow from '../../assets/images/bean_yellow.png';

const imagesAssets = [
    backyard,
    beanBlue,
    beanDead,
    beanGreen,
    beanOrange,
    beanPink,
    beanPurple,
    beanRed,
    beanWhite,
    beanYellow
];
const imagesNames = ['backyard', 'bean1', 'beanDead', 'bean2', 'bean3', 'bean4', 'bean5', 'bean6', 'bean7', 'bean8'];

export default () => {
    const obj = new Phaser.State();
    obj.preload = function () {
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
        this.preloadBar.anchor.setTo(0.5);
        this.preloadBar.scale.setTo(100, 1);
        this.load.setPreloadSprite(this.preloadBar);

        this.load.images(imagesNames, imagesAssets);
    };
    obj.create = function () {
        this.state.start('Game');
    };
    return obj;
};
