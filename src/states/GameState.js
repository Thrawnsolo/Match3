import Phaser from 'phaser';
import Block from '../prefabs/Block';
import Board from '../prefabs/Board';

export default () => {
    const obj = new Phaser.State();
    obj.init = function () {
        this.GAME_CONSTANTS = {
            NUM_ROWS: 8,
            NUM_COLS: 8,
            NUM_RESERVE_ROW: 8,
            NUM_VARIATIONS: 6,
            BLOCK_SIZE: 35,
            ANIMATION_TIME: 200
        };
    };

    obj.create = function () {
        this.background = this.add.sprite(0, 0, 'backyard');
        this.blocks = this.add.group();
        this.board = Board(
            this,
            this.GAME_CONSTANTS.NUM_ROWS,
            this.GAME_CONSTANTS.NUM_COLS,
            this.GAME_CONSTANTS.NUM_VARIATIONS
        );
        this.board.init();
        this.drawBoard();
    };

    obj.createBlock = function (x, y, data) {
        let block = this.blocks.getFirstExists(false);
        if (!block) {
            block = Block(this, x, y, data);
            this.blocks.add(block);
        } else {
            block.reset(x, y, data);
        }
        return block;
    };

    obj.drawBoard = function () {
        let x;
        let y;
        let square;
        const blockSize = this.GAME_CONSTANTS.BLOCK_SIZE + 4;
        const squareBitmap = this.add.bitmapData(blockSize, blockSize);
        squareBitmap.ctx.fillStyle = '#000';
        squareBitmap.ctx.fillRect(0, 0, blockSize, blockSize);
        for (let i = 0; i < this.GAME_CONSTANTS.NUM_ROWS; i++) {
            for (let j = 0; j < this.GAME_CONSTANTS.NUM_COLS; j++) {
                x = 36 + (j * (this.GAME_CONSTANTS.BLOCK_SIZE + 6));
                y = 150 + (i * (this.GAME_CONSTANTS.BLOCK_SIZE + 6));
                square = this.add.sprite(x, y, squareBitmap);
                square.anchor.setTo(0.5);
                square.alpha = 0.2;
                this.createBlock(x, y, { asset: `bean${this.board.grid[i][j]}`, row: i, col: j });
            }
        }
        this.game.world.bringToTop(this.blocks);
    };

    obj.getBlockFromColRow = function (position) {
        let foundBlock = null;
        this.blocks.forEachAlive((block) => {
            if (block.row === position.row && block.col === position.col) {
                foundBlock = block;
            }
        });
        return foundBlock;
    };

    obj.dropBlock = function (sourceRow, targetRow, col) {
        const block = this.getBlockFromColRow({ row: sourceRow, col });
        const targetY = 150 + (targetRow * (this.GAME_CONSTANTS.BLOCK_SIZE + 6));
        block.row = targetRow;
        const blockMovement = this.game.add.tween(block);
        blockMovement.to({ y: targetY }, this.GAME_CONSTANTS.ANIMATION_TIME);
        blockMovement.start();
    };

    obj.dropReserveBlock = function (sourceRow, targetRow, col) {
        const blockSize = this.GAME_CONSTANTS.BLOCK_SIZE + 6;
        const x = 36 + (col * blockSize);
        const y = -blockSize * (this.GAME_CONSTANTS.NUM_RESERVE_ROW + (sourceRow * blockSize));
        const block = this.createBlock(x, y, { asset: `bean${this.board.grid[targetRow][col]}`, row: targetRow, col });
        const targetY = 150 + (targetRow * blockSize);
        const blockMovement = this.game.add.tween(block);
        blockMovement.to({ y: targetY }, this.GAME_CONSTANTS.ANIMATION_TIME);
        blockMovement.start();
    };

    obj.swapBlocks = function (block1, block2) {
        const block1Movement = this.game.add.tween(block1);
        const block2Movement = this.game.add.tween(block2);

        block1.scale.setTo(1);

        block1Movement.to({ x: block2.x, y: block2.y }, this.GAME_CONSTANTS.ANIMATION_TIME);
        block2Movement.to({ x: block1.x, y: block1.y }, this.GAME_CONSTANTS.ANIMATION_TIME);

        block1Movement.start();
        block2Movement.start();
        block2Movement.onComplete.add(() => {
            const { board } = this;
            board.swap(block1, block2);
            const chains = board.findAllChains();
            if (!this.isReversingSwap) {
                if (chains.length > 0) {
                    this.updateBoard();
                } else {
                    this.isReversingSwap = true;
                    this.swapBlocks(block1, block2);
                }
            } else {
                this.isReversingSwap = false;
                this.clearSelection();
            }
        });
    };

    obj.pickBlock = function (block) {
        if (!this.isBoardBlocked) {
            if (!this.selectedBlock) {
                block.scale.setTo(1.5);
                this.selectedBlock = block;
            } else {
                this.targetBlock = block;
                if (this.board.checkAdjacent(this.selectedBlock, this.targetBlock)) {
                    this.isBoardBlocked = true;
                    this.swapBlocks(this.selectedBlock, this.targetBlock);
                } else {
                    this.clearSelection();
                }
            }
        }
    };

    obj.clearSelection = function () {
        this.isBoardBlocked = false;
        this.blocks.setAll('scale.x', 1);
        this.blocks.setAll('scale.y', 1);
        this.selectedBlock = null;
    };

    obj.updateBoard = function () {
        this.board.clearChains();
        this.board.updateGrid();
        this.game.time.events.add(this.GAME_CONSTANTS.ANIMATION_TIME, () => {
            const chains = this.board.findAllChains();
            if (chains.length > 0) {
                this.updateBoard();
            } else {
                this.clearSelection();
            }
        });
    };

    return obj;
};
