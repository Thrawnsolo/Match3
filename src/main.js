/* eslint-disable no-unused-vars */
/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import PIXI from 'pixi';
import P2 from 'p2';
import Phaser from 'phaser';
import Boot from './states/BootState';
import Preload from './states/PreloadState';
import Game from './states/GameState';

const Match3 = {};
Match3.game = new Phaser.Game(360, 640, Phaser.AUTO);
Match3.game.state.add('Boot', Boot());
Match3.game.state.add('Preload', Preload());
Match3.game.state.add('Game', Game());
Match3.game.state.start('Boot');
