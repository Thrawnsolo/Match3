export default (state, rows, cols, blockVariations) => {
    const obj = {};

    obj.state = state;
    obj.rows = rows;
    obj.cols = cols;
    obj.blockVariations = blockVariations;
    obj.grid = [];
    obj.reserveGrid = [];
    obj.NUM_RESERVE_ROW = state.GAME_CONSTANTS.NUM_RESERVE_ROW;

    const initGrid = (grid, rowNumber, colNumber) => {
        for (let i = 0; i < rowNumber; i++) {
            grid.push([]);
            for (let j = 0; j < colNumber; j++) {
                grid[i].push(0);
            }
        }
    };

    const isChained = (block, grid) => {
        let chained = false;
        const { row, col } = block;
        const variation = grid[row][col];


        // left
        if (variation === grid[row][col - 1] && variation === grid[row][col - 2]) {
            chained = true;
        }

        // right
        if (variation === grid[row][col + 1] && variation === grid[row][col + 2]) {
            chained = true;
        }

        // up
        if (grid[row - 2]) {
            if (variation === grid[row-1][col] && variation === grid[row - 2][col]) {
                chained = true;
            }
        }

        // down
        if (grid[row + 2]) {
            if (variation === grid[row + 1][col] && variation === grid[row + 2][col]) {
                chained = true;
            }
        }

        // center - horizontal
        if (variation === grid[row][col - 1] && variation === grid[row][col + 1]) {
            chained = true;
        }

        // center - vertical
        if (grid[row + 1] && grid[row - 1]) {
            if (variation === grid[row + 1][col] && variation === grid[row - 1][col]) {
                chained = true;
            }
        }
        return chained;
    };

    obj.findAllChains = function () {
        const chained = [];
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < cols; j++) {
                const block = { row: i, col: j };
                if (isChained(block, obj.grid)) {
                    chained.push(block);
                }
            }
        }
        return chained;
    };

    initGrid(obj.grid, rows, cols);
    initGrid(obj.reserveGrid, obj.NUM_RESERVE_ROW, cols);

    const populateGrid = function () {
        let variation;
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < cols; j++) {
                variation = Math.floor(Math.random() * obj.blockVariations) + 1;
                obj.grid[i][j] = variation;
            }
        }

        const chains = obj.findAllChains();
        if (chains.length > 0) {
            populateGrid();
        }
    };

    const populateReserveGrid = function () {
        let variation;
        for (let i = 0; i < obj.NUM_RESERVE_ROW; i++) {
            for (let j = 0; j < cols; j++) {
                variation = Math.floor(Math.random() * obj.blockVariations) + 1;
                obj.reserveGrid[i][j] = variation;
            }
        }
    };

    obj.init = function () {
        populateGrid();
        populateReserveGrid();
    };

    obj.swap = function (source, target) {
        const fnSource = source;
        const fnTarget = target;
        const temp = this.grid[fnTarget.row][fnTarget.col];
        const tempPosition = { row: fnSource.row, col: fnSource.col };
        this.grid[fnTarget.row][fnTarget.col] = this.grid[fnSource.row][fnSource.col];
        this.grid[fnSource.row][fnSource.col] = temp;

        fnSource.row = fnTarget.row;
        fnSource.col = fnTarget.col;
        fnTarget.row = tempPosition.row;
        fnTarget.col = tempPosition.col;
    };

    obj.checkAdjacent = function (source, target) {
        const diffRow = Math.abs(source.row - target.row);
        const diffCol = Math.abs(source.col - target.col);
        return ((diffRow === 1 && diffCol === 0) || (diffRow === 0 && diffCol === 1));
    };

    obj.clearChains = function () {
        const chainedBlocks = obj.findAllChains();
        chainedBlocks.forEach((block) => {
            this.grid[block.row][block.col] = 0;
            this.state.getBlockFromColRow(block).kill();
        });
    };

    obj.dropBlock = function (sourceRow, targetRow, col) {
        const { grid } = this;
        grid[targetRow][col] = grid[sourceRow][col];
        grid[sourceRow][col] = 0;
        this.state.dropBlock(sourceRow, targetRow, col);
    };

    obj.dropReserveBlock = function (sourceRow, targetRow, col) {
        const { grid, reserveGrid } = this;
        grid[targetRow][col] = reserveGrid[sourceRow][col];
        reserveGrid[sourceRow][col] = 0;
        this.state.dropReserveBlock(sourceRow, targetRow, col);
    };

    obj.updateGrid = function () {
        let foundBlock = false;
        // Iterate through all the blocks
        for (let i = this.rows - 1; i >= 0; i--) {
            for (let j = 0; j < this.cols; j++) {
                // If block = 0, climb up to get a non zero
                if (this.grid[i][j] === 0) {
                    foundBlock = false;
                    for (let k = i - 1; k >= 0; k--) {
                        if (this.grid[k][j] > 0) {
                            this.dropBlock(k, i, j);
                            foundBlock = true;
                            break;
                        }
                    }
                    if (!foundBlock) {
                        // check the reserveGrid
                        for (let k = this.NUM_RESERVE_ROW - 1; k >= 0; k--) {
                            if (this.reserveGrid[k][j] > 0) {
                                this.dropReserveBlock(k, i, j);
                                break;
                            }
                        }
                    }
                }
            }
        }
        populateReserveGrid();
    };
    return obj;
};
