import Phaser from 'phaser';

let parentReset;
let parentKill;
export default (state, x, y, data) => {
    const obj = new Phaser.Sprite(state.game, x, y, data.asset);
    parentReset = obj.reset;
    parentKill = obj.kill;
    obj.game = state.game;
    obj.state = state;
    obj.row = data.row;
    obj.col = data.col;
    obj.anchor.setTo(0.5);
    obj.inputEnabled = true;
    obj.events.onInputDown.add(state.pickBlock, obj.state);

    obj.reset = function (blockX, blockY, blockData) {
        parentReset.call(this, blockX, blockY);
        this.loadTexture(blockData.asset);
        this.row = blockData.row;
        this.col = blockData.col;
    };

    obj.kill = function () {
        this.loadTexture('beanDead');
        this.col = null;
        this.row = null;

        this.game.time.events.add(this.state.GAME_CONSTANTS.ANIMATION_TIME / 2, () => {
            parentKill.call(this);
        });
    };

    return obj;
};
